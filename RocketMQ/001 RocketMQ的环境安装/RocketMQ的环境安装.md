## 下载安装包
http://rocketmq.apache.org/dowloading/releases/
从这里我们可以下载对应的安装包,现在选定的版本为4.4.0 release版本.

## 安装包对应的目录结构
我们首先看看安装包的基本的目录结构,然后分析一下目录结构之中我们常用的内容.
![目录结构](./images/RocketMQ的目录结构.png)
[1] bin目录下存放我们以后需要使用的脚本文件,包含启动等操作的内容.
[2]conf目录:存放我们的配置文件内容,以后我们对中间件的调整都是从这个目录进行的.

## 启动RocketMQ
我们首先在bin目录下找到启动文件,RocketMQ的启动文件我们需要增加一些修改,原因是RocketMQ的启动需要的内存比较多,我们考虑到时测试的环境下,我们应该将这一部分的消耗调低.
[1]首先找到runserver.cmd脚本,我们使用编辑器打开这个文件.
```
if not exist "%JAVA_HOME%\bin\java.exe" echo Please set the JAVA_HOME variable in your environment, We need java(x64)! & EXIT /B 1
set "JAVA=%JAVA_HOME%\bin\java.exe"

setlocal

set BASE_DIR=%~dp0
set BASE_DIR=%BASE_DIR:~0,-1%
for %%d in (%BASE_DIR%) do set BASE_DIR=%%~dpd

set CLASSPATH=.;%BASE_DIR%conf;%CLASSPATH%

set "JAVA_OPT=%JAVA_OPT% -server -Xms2g -Xmx2g -Xmn1g -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
set "JAVA_OPT=%JAVA_OPT% -XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:CMSInitiatingOccupancyFraction=70 -XX:+CMSParallelRemarkEnabled -XX:SoftRefLRUPolicyMSPerMB=0 -XX:+CMSClassUnloadingEnabled -XX:SurvivorRatio=8 -XX:-UseParNewGC"
set "JAVA_OPT=%JAVA_OPT% -verbose:gc -Xloggc:"%USERPROFILE%\rmq_srv_gc.log" -XX:+PrintGCDetails"
set "JAVA_OPT=%JAVA_OPT% -XX:-OmitStackTraceInFastThrow"
set "JAVA_OPT=%JAVA_OPT% -XX:-UseLargePages"
set "JAVA_OPT=%JAVA_OPT% -Djava.ext.dirs=%BASE_DIR%lib"
set "JAVA_OPT=%JAVA_OPT% -cp "%CLASSPATH%""

"%JAVA%" %JAVA_OPT% %*
```
从脚本的内容看,我们是需要设置JAVA_HOME内容的,不然就无法启动ROcketMQ.当前的环境下,我们已经配置了JAVA_HOME环境变量了.
```
set "JAVA_OPT=%JAVA_OPT% -server -Xms2g -Xmx2g -Xmn1g -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
```
我们需要注意到上面的一段内容,这一段内容表示启动的时候需要的内存,在这里,我们对其进行调整.
```
set "JAVA_OPT=%JAVA_OPT% -server -Xms512m -Xmx512m -Xmn256m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
```
修改成 512m 512m 256m即可.

[2] 修改borker的启动脚本.
```
set "JAVA_OPT=%JAVA_OPT% -server -Xms512m -Xmx512m -Xmn256m"
```
找到对应的内存修改,修改成上述的内容即可.
[3] 配置RocketMQ的环境变量
我们在环境变量里面配置一个ROCKETMQ_HOME的环境变量,其目录指向bin之前的目录.
[4] 使用start mqnamesrv.cmd 启动nameserver.
![启动nameServer](./images/启动nameServer.png) 
如果出现该内容,表示我们的nameServer已经启动成功.
[5]使用 start mqbroker启动broker端.
![启动broker](./images/启动broker.png)

至此,我们的rocketmq已经征程的启动了.

## 配置rocketmq-console控制台
我们首先访问https://github.com/apache/rocketmq-externals/tree/release-rocketmq-console-1.0.0,然后下载对应的代码(其中rocket-console是一个分支).
在下载的代码之中,我们进入到rocket-console的目录下,执行如下的命令进行打包操作.
mvn clean package -Dmaven.test.skip=true
这样,就会在target文件下出现我们最终需要的打包文件了.
```
 java -jar target/rocketmq-console-ng-1.0.0.jar --rocketmq.config.namesrvAddr=‘192.168.x.xx:9876’
```
我们执行上述的命令,就可以得到一个console了.
默认情况下,我们只需要在浏览器上访问:http://localhost:8080就可以访问到控制台了.