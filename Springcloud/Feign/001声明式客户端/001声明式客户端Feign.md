## 声明式客户端
在之前,我们调用微服务的时候,使用的是Ribbon加上Resttemplate完成的,当我们在使用的时候,发现,dubbo那种接口的方式会更加的友好,因为,我们的IDE会帮助我们进行补全.于是,Feign出现了,它默认也集成了Ribbon,也就是说,它天生也具体负载均衡的能力.

## Fign的基本使用
在使用Feign之前,我们需要首先引入jar.如下:
```
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>
```
然后,我们需要编写配置类.
```
/**
 * 通过Feign,标示调用的
 */
@FeignClient(value = "producer")
public interface ProducerFeign {

    @GetMapping("hello")
    public String hello();
}
```
我们编写了一个配置类,实际上就我们以后需要使用的Feign的接口调用类,但是这个类现在仅仅只是接口,因此,我们需要对其进行声明.
```
@SpringBootApplication
@EnableFeignClients()
public class ConsumerStarter {
```
我们添加了一个@EnableFeignClients注解,通过这个注解,它会找到其子包之中的所有的标注@Feign的子接口,对其进行实例化,添加到IOC容器之中,在后面,我们就可以对其进行访问了.
Feign的客户端调用方法
```
    @Resource
    private ProducerFeign producerFeign;

    @GetMapping("feignHello")
    public String feignHello(){
        return this.producerFeign.hello();
    }
```
我们使用声明式的Feign进行进行远程微服务的调用,发现,和之前的进程内的嗲用没有任何的区别.
