## Feign的架构说明
在之前,我们看到了Feign的基本使用,我们也知道Feign在使用的时候是比较简单的,本次,我们来说明一下Feign的基本架构和扩展点,方便后面使用的时候进行扩展.

## Feign的基本架构
在开发微服务的时候,我们会在主类上添加一个@EnableFeignClients的注解,然后指定其扫包的范围.然后soringcloud就会帮助我们去寻找标记为@FeignClient的接口,为其创建一个代理对象放入到IOC容器之中,最终,当我们调用的时候,实际上是一个代理对象进行的调用.

## 替换Feign的底层实现
默认情况下,Feign会使用java底层的URLconnnection去i将女性http的通信,当然这种方式的性能比较低,最起码的连接池等功能都不存在.因此,我们在使用的使用一般都需要一个更好的http通信库,现在比较主流的http库有httpclient或者和okhttp.
```
feign.okhttp.enabled=true
```
我们在配置文件中指定了该库,就表示我们现在已经可以使用okhttp了,但是我们需要引入对应的依赖内容.
```

<dependency>
    <groupId>com.squareup.okhttp3</groupId>
    <artifactId>okhttp</artifactId>
    <version>4.1.0</version>
</dependency>

```
很方便的,我们可以通过这种方式来替换Feign的底层实现.

## Feign的参数问题
Feign默认情况下可以适配Springmvc的大部分注解,但是比较起来,Feign之中的注解会变大更加严格些.我们应该对每一个参数都使用@RequestParam进行标注,放置出现错误.
通过,在传递多参数的时候,Feign同样会出现一些问题.
一个常见的问题就是通过Feign传递pojo的时候会出现问题.
这里给出一个比较优雅的解决方案.

默认的情况下,feign是不支持Feign的对参数的pojo模式的调用.
当我们使用get请求的时候,我们需要在pojo的上面添加一个@SpringQueryMap.

对于post的方式,我们可以使用@RequestBody.来解决这个问题.


## Feign的性能优化
Feign的性能是比较差的,因此,我们一般情况下都会尽量的优化.
[1]Feign之中,默认情况下不会使用连接池,我们一般会使用okhttp进行发送请求.
[2]请求响应压缩等功能,实际上这些功能对性能的影响还是比较小的.

## Feign的文件上传
```
<dependency>
    <groupId>io.github.openfeign.form</groupId>
    <artifactId>feign-form</artifactId>
	<version>3.0.3</version>
</dependency>
<dependency>
	<groupId>io.github.openfeign.form</groupId>
	<artifactId>feign-form-spring</artifactId>
	<version>3.0.3</version>
</dependency>
```
```
@FeignClient(name = "ms-content-sample", configuration = UploadFeignClient.MultipartSupportConfig.class)
public interface UploadFeignClient {
    @RequestMapping(value = "/upload", method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE},
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    String handleFileUpload(@RequestPart(value = "file") MultipartFile file);

    class MultipartSupportConfig {
        @Bean
        public Encoder feignFormEncoder() {
            return new SpringFormEncoder();
        }
    }
}
```
因为文件上传的时间比较长,默认情况下,我们应该设置一个大的文件上传请求时间.

另外,我们可以将文件上传作为一个微服务进行支持,这个时候,我们就不会出现使用Feign进行文件上传的请求了.

## 使用Feign实现表单提交
```
<dependency>
  <groupId>io.github.openfeign.form</groupId>
  <artifactId>feign-form</artifactId>
  <version>3.2.2</version>
</dependency>
<dependency>
  <groupId>io.github.openfeign.form</groupId>
  <artifactId>feign-form-spring</artifactId>
  <version>3.2.2</version>
</dependency>
```
```
@FeignClient(name = "xxx", url = "http://www.itmuch.com/", configuration = TestFeignClient.FormSupportConfig.class)
public interface TestFeignClient {
    @PostMapping(value = "/test",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE}
            )
    void post(Map<String, ?> queryParam);

    class FormSupportConfig {
        @Autowired
        private ObjectFactory<HttpMessageConverters> messageConverters;
        // new一个form编码器，实现支持form表单提交
        @Bean
        public Encoder feignFormEncoder() {
            return new SpringFormEncoder(new SpringEncoder(messageConverters));
        }
        // 开启Feign的日志
        @Bean
        public Logger.Level logger() {
            return Logger.Level.FULL;
        }
    }
}
```


