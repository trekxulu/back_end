## Ribbon的介绍
我们在之前已经完成了服务的注册,我们也知道我们的微服务一旦上线就肯定是集群部署的,当涉及到微服务之间调用的使用,就会出现一个负载均衡的问题.简单说,就是相同的微服务很多,我们的客户端到底调用哪一个微服务呢?
在Nacos-discovery的客户端的之中,默认集成了Ribbon.Ribbon作为一个客户端负载均衡器,可以帮助我们的微服务决定到底调用哪一个微服务.

## Ribbon的集成问题
Ribbon默认被集成到了服务发现组件之中,我们一般会在springcloud之中使用RestTemplate组件进行服务之间的调用.
```
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate(());
    }
```
我们装配RestTemplate的时候,可以在标准为一个@LoadBalanced来标示为一个负载均衡组件.
当我们标示了该注解之后,RestTemplate的发送请求的时候,会帮助我们将对应的微服务的名称转换为对应的端口号和ip.在Ribbon之中,默认维护了一个对应的服务列表,这个列表的内容是从服务注册中心之中发现的,Ribbon维护这个服务列表的可用性,因此Ribbon会知道到底需要调用哪一个组件.
Ribbon同时是一个可以高度配置的组件,我们可以通过配置不同的内容实现Ribbon的定制.

## Ribbon的基本使用
[1]配置客户端,也就是消费者.
```
server.port=8082
spring.application.name=consumer
spring.cloud.nacos.discovery.server-addr=127.0.0.1:8848
management.endpoints.web.exposure.include=*
```

```
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Resource
    private RestTemplate restTemplate;

    /**
     * 定义了一个mapping,会访问consumer的微服务
     * 注意,我们远程的地址之中标示的是producer,其实就是对应的微服务的名称
     * @return
     */
    @GetMapping("hello")
    public String hello(){
        return this.restTemplate.getForObject("http://producer/hello",String.class);
    }
```
我们对RestTemplate之中添加了一个@LoadBalanced注解,表示使用Ribbon的负载均衡能力.
通过我们编写了一个接口,这个接口的内容是访问远程的接口的内容.但是我们的编写的url的地址实际上是一个服务的名称,Ribbon会帮助我们实现这个服务名称的转换.

[2]编写消费者
```
@RestController
public class HelloController {

    @Value("${server.port}")
    private String serverPort;


    /**
     * 产出一个接口,返回的是一个server-port的内容,我们会使用这种方式验证负载均衡的实现.
     * @return
     */
    @GetMapping("hello")
    public String serverPort(){
        return this.serverPort;
    }

}
```
我们编写了一个十分的简单的接口内容,返回的就是对应的服务的端口号,之所以返回的是端口号,是我们想去验证Ribbon的负载均衡.
[3]服务的启动
(1)首先启动nacos服务注册中心
(2)启动消费者,我们本次启动一个消费者实例即可.
(3)启动生产者,我们启动两个实例,端口号分别是7000和7001端口.
(4)访问nacos的控制台
![ribbon-nacos](./images/ribbon-nacos.png)
我们可以发现我们的服务已经注册到了nacos上面.
(5)验证ribbon的负载均衡
当我们在浏览器之中访问http://localhost:8082/hello的时候,我们不停的访问该接口,其实就是消费者的接口内容,我们发现返回值不停的在7000和7001之间进行切换.这就说明了我们的ribbon的负载均衡已经实现了,之所以调用的结果是有规律的,实际上是由于ribbon的负载均衡策略导致的,在后面,我们会覆写这些策略,然后配置成为我们喜欢的模式.