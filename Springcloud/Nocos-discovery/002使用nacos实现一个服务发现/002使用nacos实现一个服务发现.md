## springcloud的环境搭建
本次,我们使用nacos完成springcloud的微服务注册到nacos服务注册中心的实验.

## 版本的要求
```
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Greenwich.SR1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>2.1.0.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```
springcloud-alibab已经孵化成功,现在我们就去使用springcloud-alibaba的正式版本了.
我们建立一个producer的子项目,其中我们引入如下的依赖内容.
```
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- actuator-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
```
我们在上述的内容之中引入web模块和nacos-client模块的内容.

## 代码的编写
按照springcloud的三板斧的要求
[1]引入pom的配置--已完成
[2]添加注解
```
@SpringBootApplication
@EnableDiscoveryClient
public class Producer 
{
    public static void main( String[] args )
    {
        SpringApplication.run(Producer.class);
    }
}
```

我们在主启动类之中添加服务发现的注解内容.
[3]添加配置
```
server:
  port: 7000

spring:
  application:
    name: producer
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848

```
比较之前的eureka服务发现组件,我们的基础配置变的少了很多.我们只需要配置一个nacos的地址即可.

## 实验的验证
我们启动该服务(保证nacos也正常的启动着),我们在正常启动之后,我们观察nacos的控制台,会发现如下的内容.
![nacos服务发现](./iamges/nacos服务发现.png).
我们可以发现,我们的微服务producer已经注册到了naocs上面,也就说明我们使用nacos的服务发现客户端已经可以正常的注册到了naocs上面.
