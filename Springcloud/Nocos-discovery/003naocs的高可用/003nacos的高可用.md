## 高可用的设计目标
对于naocs来说,我们会使用其作为服务的注册中心,该组件作为微服务的核心组件,我们必须要保证其是高可用的.
nacos为了保证高可用,实现了如下的策略保证.
[1]信息的持久化,我们可以配置其将数据保存到mysql之中.
[2]nacos的集群设置
下面,我们会按照这两方面的内容实现nacos的高可用模式.
![nacos高可用集群模式](./images/nacos集群.png)

## 持久化的mysql模式

nacos天生就具有持久化到mysql的机制,我们通过配置内容,可以将nacos存储的信息同步到mysql之中,这样当发生异常情况的时候,nacos的数据不至于全部丢失.

一般情况下,我们的mysql也应该是集群模式,但是在本次的模式下,我们仅仅使用单机的mysql即可完成任务,mysql在当前的环境下实际上发生故障的机会十分的小.

[1]初始化sql脚本
在nacos的conf文件之中,实际上存在对应的sql脚本,我们需要执行该脚本到mysql之中,创建mysql需要的环境.
我们在数据库之中创建一个nacos的库,然后执行该脚本,运行之后的结果如下:
![nacos的数据库](./images/nacos脚本.png)


[2]配置核心配置文件的内容都存放在application.properties之中,我们需要修改这一部分的内容.
```
db.num=1
db.url.0=jdbc:mysql://localhost:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=root
db.password=trek
```
我们在配置文件之中,增加这一部分的内容,这一部分的内容都存放在nacos的实例配置文件之中.

## 配置多nacos
我们可以在nacos的conf文件夹下找到如下的配置文件.
我们可以找到一个cluster的配置文件
```
#it is ip
#example
localhost:8848
localhost:8849
localhost:8850
```
我们增加上述的配置内容,实际上就是配置多台主机.
我们分别启动上述的主机.

## 配置nginx
```
upstream nacos {
  server 127.0.0.1:8848;
  server 127.0.0.1:8849;
  server 127.0.0.1:8850;
}

server {
  listen 80;
  server_name  localhost;
  location /nacos/ {
    proxy_pass http://nacos/nacos/;
  }
}
```
我们现在使用nginx帮助反向代理了nacos的三个服务器,因此nginx会帮助我们负载均衡到这些主机上.

## 配置springcloud服务发现
```
spring:
  cloud:
    nacos:
      discovery:
        server-addr: nginx绑定的域名:80
```
现在,我们在nacos的服务发现中填写的地址就可以是nginx的地址了,nginx会帮助我们进行负载均衡.
