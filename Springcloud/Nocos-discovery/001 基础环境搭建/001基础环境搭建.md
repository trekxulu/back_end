## Nacos服务发现组件
之前的springcloud版本之中,我们一般会使用eureka
或者consul来实现服务发现,当前的情况下,eureka宣布不再进行维护,consul天生存在问题,因此我们需要一款新的组件帮助实现服务发现,nacos就是一款比较优秀的服务发现组件,它已经被纳入到springcloud-alibaba套件之中.

## nacos服务发现server
nacos的server端是一个由springboot编写的程序,因此我们可以直接使用jar的方式进行启动,nacos封装了这一层的内容,可以让我们更加轻松的使用这一组件.

## 环境的搭建
[1]组件地址 :https://github.com/alibaba/nacos
[2]使用方式 : 我们下载的是一个zip包,当前的最新版本是1.0.0版本.
[3]我们解压这个软件包之后,可以发现这个软件包就是一个标准的中间包的结构.如下:
![nacos包结构](./images/nacos包结构.png)    
[4]我们关注的核心内容存在于bin目录和conf结构之中.
[5]启动nacos
```
sh startup.sh -m standalone
```
在启动的命令中,我们可以看到nacos具有集群模式,这个会在后面进行说明.
由于,本次实验是在window环境下,我们可以直接启动start.bat脚本即可.
[6]成功启动的结果
![nacos成功启动的结果](./images/nacos启动脚本.png)
我们从上述的日志消息之中可以获取到如下的信息,暴漏出来的端口号是8848端口,当前是按照单机模式进行启动的.
[7]当我们从浏览器之中访问nacos的8848的时候,会出现要求输入账号和密码的地方,默认的账号和密码都是nacos.
[8]访问nacos的首页内容
![nacos首页内容](./images/nacos服务.png)
从首页的内容之中,我们可以看到,nacos本身支持服务发现和配置管理,这也正好和nacos的名称一致,其本身就是一个服务发现和配置中心的实现.